package customervoicecheck.in

import grails.converters.JSON

class BookingApiController {

    def getInfo() {
        def bookingId = params.bookingId

        if (bookingId) {
            def booking = Booking.findByBookingRef(bookingId)
            if (booking) {
                def response = [:]
                // Fill booking related data into response
                response.put("bookingRef", booking.bookingRef)
                response.put("roomNum", booking.roomNum)
                response.put("checkIn", booking.checkInDate)
                response.put("checkOut", booking.checkOutDate)

                // Get guest data
                def guestBookings = GuestBooking.findAllByBooking(booking)
                def otherGuests = []
                guestBookings.each { guestBooking ->
                    if (guestBooking.mainGuest == true) {
                        response.put("mainGuest", "${guestBooking.guest.lastName}, ${guestBooking.guest.firstName}")
                    } else {
                        def guest = "${guestBooking.guest.lastName}, ${guestBooking.guest.firstName}"
                        otherGuests += guest
                    }
                }

                response.put("otherGuests", otherGuests)

                render response as JSON
            } else {
                response.status = 404
                render "404"
            }
        } else {
            response.status = 400
            render "400"
        }
    }
}
