package customervoicecheck.in

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/check-in")
        "500"(view:'/error')
        "404"(view:'/notFound')
    }
}
