package customervoicecheck.in

class BootStrap {
    private final firstNames = ["Diss", "Aiem", "Mie", "Kolmy", "Maineim", "John", "Sam"]
    private final lastNames = ["Natreel", "Pheik", "Meidup", "Mock", "Doe", "Smith", "Johnson"]
    private final numOfGuests = 20
    private final maxNumOfPass = 4
    private final numOfBookings = 3

    def keyGeneratorService

    def init = { servletContext ->
        // Only run this if no data is available
        if (Guest.list().size() < 1) {

            // Create mock guests
            for (int i = 0; i < numOfGuests; i++) {
                def mockGuest = new Guest()
                mockGuest.firstName = firstNames[new Random().nextInt(firstNames.size())]
                mockGuest.lastName = lastNames[new Random().nextInt(lastNames.size())]
                mockGuest.save(flush: true, failonerror: true)
            }

            // Create mock bookings
            for (int i = 0; i < numOfBookings; i++) {
                def mockBooking = new Booking()
                mockBooking.bookingRef = keyGeneratorService.getKey(6)
                mockBooking.roomNum = new Random().nextInt(400) + 1

                Date checkIn = new Date() + new Random().nextInt(6)
                mockBooking.checkInDate = checkIn
                mockBooking.checkOutDate = checkIn + new Random().nextInt(6) + 1
                mockBooking.save(flush: true, failonerror: true)
            }

            // Assign guests to bookings
            def bookings = Booking.list()
            def guests = Guest.list()

            bookings.each { booking ->
                // Create main guest
                def mainGuestBooking = new GuestBooking()

                // Create array of used guests to ensure none are repeated per booking
                def usedGuests = []
                def guest = guests[new Random().nextInt(numOfGuests)]
                usedGuests += guest
                mainGuestBooking.guest = guest
                mainGuestBooking.booking = booking
                mainGuestBooking.mainGuest = true
                mainGuestBooking.save(flush: true, failonerror: true)

                // Add passangers to the booking
                def numOfPass = new Random().nextInt(maxNumOfPass)
                for (int i = 0; i < numOfPass; i++) {
                    def passangerBooking = new GuestBooking()
                    passangerBooking.booking = booking
                    passangerBooking.guest = guests[new Random().nextInt(numOfGuests)]

                    // Ensure that none of the guests are assigned twice to one booking
                    if (usedGuests.contains(passangerBooking.guest)) {
                        while (usedGuests.contains(passangerBooking.guest)) {
                            passangerBooking.guest = guests[new Random().nextInt(numOfGuests)]
                        }
                    }

                    passangerBooking.save(flush: true, failonerror: true)
                }
            }
        }

        def guests = Guest.list()

        guests.each {guest ->
            println "${guest.firstName} ${guest.lastName}"
        }

        def bookings = Booking.list()

        bookings.each {booking ->
            // Print booking details
            println "${booking.bookingRef}: room number ${booking.roomNum}, staying from ${booking.checkInDate} till ${booking.checkOutDate}"

            // Get all the guests
            def guestBookings = GuestBooking.findAllByBooking(booking)

            guestBookings.each {guestBooking ->
                def guest = guestBooking.guest
                println "${guest.lastName}, ${guest.firstName}. Main guest: ${guestBooking.mainGuest}"
            }

        }
    }
    def destroy = {
    }
}
