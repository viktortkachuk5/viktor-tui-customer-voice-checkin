package customervoicecheck.in

class GuestBooking {
    Guest guest
    Booking booking
    Boolean mainGuest = false

    static constraints = {
        guest nullable: false
        booking nullable: false
        mainGuest nullable: false
    }
}
