package customervoicecheck.in

class Booking {
    String bookingRef
    Integer roomNum
    Date checkInDate
    Date checkOutDate

    static constraints = {
        bookingRef nullable: false, blank: false
        roomNum nullable: false
        checkInDate nullable: false
        checkOutDate nullable: false
    }
}
