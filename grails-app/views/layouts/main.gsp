<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<html class="no-js" lang="">
	<head>
		%{--<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">--}%
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta content="" name="description">
		<meta content="" name="author">
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<!-- Begin iOS Specific Links & Meta Data -->
		<link rel="apple-touch-icon-precomposed" sizes="57x57" href="${resource(dir: 'images/icons', file: 'apple-touch-icon-57x57.png')}">
		<link rel="apple-touch-icon-precomposed" sizes="114x114" href="${resource(dir: 'images/icons', file: 'apple-touch-icon-114x114.png')}">
		<link rel="apple-touch-icon-precomposed" sizes="72x72" href="${resource(dir: 'images/icons', file: 'apple-touch-icon-72x72.png')}">
		<link rel="apple-touch-icon-precomposed" sizes="144x144" href="${resource(dir: 'images/icons', file: 'apple-touch-icon-114x114.png')}">
		<link rel="apple-touch-icon-precomposed" sizes="60x60" href="${resource(dir: 'images/icons', file: 'apple-touch-icon-60x60.png')}">
		<link rel="apple-touch-icon-precomposed" sizes="120x120" href="${resource(dir: 'images/icons', file: 'apple-touch-icon-120x120.png')}">
		<link rel="apple-touch-icon-precomposed" sizes="76x76" href="${resource(dir: 'images/icons', file: 'apple-touch-icon-76x76.png')}">
		<link rel="apple-touch-icon-precomposed" sizes="152x152" href="${resource(dir: 'images/icons', file: 'apple-touch-icon-152x152.png')}">
		<meta name="apple-mobile-web-app-title" content="CommonUX">
		<!-- End iOS Specific Links & Meta Data -->
		<!-- Begin Android Specific Links & Meta Data -->
		<link rel="icon" type="image/png" href="${resource(dir: 'images', file: 'favicon-196x196.png')}" sizes="196x196">
		<link rel="icon" type="image/png" href="${resource(dir: 'images', file: 'favicon-192x192.png')}" sizes="192x192">
		<link rel="icon" type="image/png" href="${resource(dir: 'images', file: 'favicon-96x96.png')}" sizes="96x96">
		<link rel="icon" type="image/png" href="${resource(dir: 'images', file: 'favicon-32x32.png')}" sizes="32x32">
		<link rel="icon" type="image/png" href="${resource(dir: 'images', file: 'favicon-16x16.png')}" sizes="16x16">
		<link rel="icon" type="image/png" href="${resource(dir: 'images', file: 'favicon-128x128.png')}" sizes="128x128">
		<link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<link rel="icon" href="${resource(dir: 'images', file: 'favicon.ico')}" type="image/x-icon">
		<meta name="theme-color" content="#A0C8E6">
		<!-- End Android Specific Links & Meta Data -->
		<!-- Begin Internet Explorer Specific Meta Data -->
		<meta name="application-name" content="CommonUX">
		<meta name="msapplication-TileColor" content="#A0C8E6">
		<meta name="msapplication-TileImage" content="${resource(dir: 'images', file: 'mstile-144x144.png')}">
		<meta name="msapplication-square70x70logo" content="${resource(dir: 'images', file: 'mstile-70x70.png')}">
		<meta name="msapplication-square150x150logo" content="${resource(dir: 'images', file: 'mstile-150x150.png')}">
		<meta name="msapplication-wide310x150logo" content="${resource(dir: 'images', file: 'mstile-310x150.png')}">
		<meta name="msapplication-square310x310logo" content="${resource(dir: 'images', file: 'mstile-310x310.png')}">
		%{--<asset:stylesheet src="application.css"/>--}%
		%{--<asset:javascript src="application.js"/>--}%
		<g:layoutHead/>
	</head>
	<body class="page-container-bg-solid page-header-fixed page-footer-fixed">
		<g:layoutBody/>
		<!-- Begin Footer -->
		<div class="page-footer">
			%{--<a href="#" i18n="common.privacy">Privacy</a> | --}%
			%{--<a href="#" i18n="common.terms">Terms</a> | --}%
			<a href="http://servicedesk.content.tuigroup.com/" i18n="common.contactUs" target="_blank">Contact Us</a> |
			<a href="http://servicedesk.content.tuigroup.com/" i18n="common.help" target="_blank">Help</a>
			<div class="copyright"><g:message code="survey.footer.p" /> | <span id="tui-date-time"></span></div>
		</div>
		<!-- End Footer -->
		<script>
		$(document).ready(function() {
			// Initialize Date/Time
			var NowMoment = moment().format('ddd Do MMM YYYY');
			var eDisplayMoment = document.getElementById('tui-date-time');
			eDisplayMoment.innerHTML = NowMoment;
		});
		</script>
	</body>
</html>
