<%--
  Created by IntelliJ IDEA.
  User: TTHVTX
  Date: 08/05/2017
  Time: 10:21
--%>
<!DOCTYPE html>
<!--

                                         :osyso:
                                       +yyyyyyyo
                                       oyyyyyyyo
                                       `+sssss+`
                                         `...`
-++++++++++++++/`
syyyyyyyyyyyyyyy+
osysssyyyyysoooo:
`....:yyyyy:`                          `///-
    syyyyo                          .oyyys`
    :yyyyy/                        .syyyy+
     oyyyyy:                      -syyyyo`
     `syyyyy/`                  `/syyyys.
      .syyyyyo:`              `:syyyyyo.
       .oyyyyyys/.`        `-+syyyyyy+`
        `+yyyyyyysso+////+osyyyyyyyo-
          .oyyyyyyyyyyyyyyyyyyyyyo-
            ./syyyyyyyyyyyyyyys/.
               .:+ossyyyso+/-`

-->
<!--[if IE 8]>
<html lang="en" class="ie8">
<![endif]-->
<!--[if IE 9]>
<html lang="en" class="ie9">
<![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<html class="no-js" lang="">
<head>
    <meta name="layout" content="main" />
    <meta charset="utf-8">
    <title>Voice Check-In</title>
    <link href="${assetPath(src: 'main.css')}" rel="stylesheet">
    <asset:javascript src="modernizr.js" />
</head>
<body>
<!--[if lt IE 10]>
    <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
<!-- Begin Header -->
<div class="page-header navbar navbar-fixed-top">
    <!-- Begin Header Inner -->
    <div class="page-header-inner">
        <!-- Begin Logo -->
        <div class="page-logo">
            <h1 class="page-heading logo-default">
                <figure>
                    <a href="${resource(dir: 'images', file: 'tui_group_logo.svg')}">
                        <div class="bg-svg">

                        </div>
                    </a>
                </figure>
                <span>Customer Voice Check-In</span>
            </h1>
        </div>
    </div>
    <!-- End Logo -->
</div>
<!-- End Header Inner -->
<!-- End Header -->
<!-- Begin Header & Content Divider -->
<div class="clearfix"></div>
<!-- End Header & Content Divider -->
<!-- Begin Container -->
<div class="page-container ">
    <!-- Begin Content -->
    <div class="page-content-wrapper">
        <!-- Begin Content Body -->
        <div class="page-content no-sidebar">
            <!-- Begin Page Base Content -->
            <div class="row">
                <div class="col-md-8">
                    Say "Hello"
                </div>
                <div class="col-md-4">
                    <button type="button" onclick="enableBookingSearch()" id="hello-button" class="btn btn-success"><i class="fa fa-save"></i> Or click me</button>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    Say your booking reference or search it manually
                </div>
                <div class="col-md-3">
                    <input id="bookingSearch" type="text" disabled/><button type="button" onclick="search()" id="search-button" class="btn btn-success" disabled><i class="fa fa-save"></i> Search</button>
                </div>
                <div id="booking-issue-div" class="col-md-4" hidden>
                    Try again or ask staff for assistance
                </div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    Close info/Restart
                </div>
                <div class="col-md-4">
                    <button type="button" onclick="restartCheckIn()" id="restart-button" class="btn btn-success"><i class="fa fa-save"></i> Restart</button>
                </div>
            </div>
            </div>
            <!-- End Page Base Content -->
        </div>
        <!-- End Content Body -->
    </div>
    <!-- End Content -->
</div>
<!-- End Container -->
<asset:javascript src="jquery.min.js" />
<asset:javascript src="bootstrap.min.js" />
<asset:javascript src="moment.min.js" />
<asset:javascript src="properties.js" />
<asset:javascript src="app.js" />

<script>

    function enableBookingSearch() {
        $('#bookingSearch').prop('disabled', false);
        $('#search-button').prop('disabled', false)
    }

    function disableBookingSearch() {
        $('#bookingSearch').prop('disabled', true);
        $('#search-button').prop('disabled', true)
    }

    function search() {
        var id = $('#bookingSearch').val();
        $.ajax({
            type: 'GET',
            url: '/bookingApi/getInfo?bookingId=' + id,
            dataType: 'json',
            success: function (data) {
                alert(data)
            }
        });
    }

    function restartCheckIn() {
        $('#bookingSearch').val("");
        disableBookingSearch()
    }

    $(document).ready(function() {

        try {
            var recognition = new webkitSpeechRecognition();
        } catch(e) {
            var recognition = Object;
        }
        recognition.continuous = true;
        recognition.interimResults = true;

        var interimResult = '';
        var textArea = $('#bookingSearch');
        var textAreaID = 'bookingSearch';

        textArea.focus();
        recognition.start();

        recognition.onresult = function (event) {
            var pos = textArea.getCursorPosition() - interimResult.length;
            textArea.val(textArea.val().replace(interimResult, ''));
            interimResult = '';
            textArea.setCursorPosition(pos);
            for (var i = event.resultIndex; i < event.results.length; ++i) {
                if (event.results[i].isFinal) {
                    insertAtCaret(textAreaID, event.results[i][0].transcript);
                } else {
                    isFinished = false;
                    insertAtCaret(textAreaID, event.results[i][0].transcript + '\u200B');
                    interimResult += event.results[i][0].transcript + '\u200B';
                }
            }
        };
    });

    function insertAtCaret (areaId,text) {
        var txtarea = document.getElementById(areaId);
        var scrollPos = txtarea.scrollTop;
        var strPos = 0;
        var br = ((txtarea.selectionStart || txtarea.selectionStart == '0') ?
            "ff" : (document.selection ? "ie" : false ) );
        if (br == "ie") {
            txtarea.focus();
            var range = document.selection.createRange();
            range.moveStart ('character', -txtarea.value.length);
            strPos = range.text.length;
        }
        else if (br == "ff") strPos = txtarea.selectionStart;

        var front = (txtarea.value).substring(0,strPos);
        var back = (txtarea.value).substring(strPos,txtarea.value.length);
        txtarea.value=front+text+back;
        strPos = strPos + text.length;
        if (br == "ie") {
            txtarea.focus();
            range = document.selection.createRange();
            range.moveStart ('character', -txtarea.value.length);
            range.moveStart ('character', strPos);
            range.moveEnd ('character', 0);
            range.select();
        }
        else if (br == "ff") {
            txtarea.selectionStart = strPos;
            txtarea.selectionEnd = strPos;
            txtarea.focus();
        }
        txtarea.scrollTop = scrollPos;
    };

    $.fn.getCursorPosition = function() {
        var el = $(this).get(0);
        var pos = 0;
        if('selectionStart' in el) {
            pos = el.selectionStart;
        } else if('selection' in document) {
            el.focus();
            var Sel = document.selection.createRange();
            var SelLength = document.selection.createRange().text.length;
            Sel.moveStart('character', -el.value.length);
            pos = Sel.text.length - SelLength;
        }
        return pos;
    };

    $.fn.setCursorPosition = function(pos) {
        if ($(this).get(0).setSelectionRange) {
            $(this).get(0).setSelectionRange(pos, pos);
        } else if ($(this).get(0).createTextRange) {
            var range = $(this).get(0).createTextRange();
            range.collapse(true);
            range.moveEnd('character', pos);
            range.moveStart('character', pos);
            range.select();
        }
    };

    (function (b, o, i, l, e, r) {
        b.GoogleAnalyticsObject = l;
        b[l] || (b[l] =
            function () {
                (b[l].q = b[l].q || []).push(arguments)
            });
        b[l].l = +new Date;
        e = o.createElement(i);
        r = o.getElementsByTagName(i)[0];
        e.src = 'https://www.google-analytics.com/analytics.js';
        r.parentNode.insertBefore(e, r)
    }(window, document, 'script', 'ga'));
    ga('create', 'UA-XXXXX-X');
    ga('send', 'pageview');


</script>

</div>

</body>
</html>