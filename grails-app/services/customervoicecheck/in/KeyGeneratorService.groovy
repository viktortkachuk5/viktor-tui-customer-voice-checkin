package customervoicecheck.in

class KeyGeneratorService {

    static transactional = false

    /**
     * This method will generate a unique random key when requested. It
     * then encodes the key using Sha1 encoding and convert the key into
     * a 40 characters hexadecimal based character key.
     * @return Key
     */
    def getKey()
    {
        return getKey(39)
    }

    /**
     * This method will generate a unique random key when requested. It
     * then encodes the key using Sha1 encoding and convert the key into
     * a 40 characters hexadecimal based character key.
     * @return Key
     */
    def getKey(int IDlength)
    {
        // declaring characters to be used for generating a key.
        String validChars ="ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890"
        int maxIndex = validChars.length()

        String resultID = ""
        // generating a random string using java random class and getting current time to be attached with the key.
        Random rnd = new Random(System.currentTimeMillis()*(new Random().nextInt()))
        for ( i in 0..IDlength ) {
            int rndPos = Math.abs(rnd.nextInt() % maxIndex);
            resultID += validChars.charAt(rndPos)
        }
        return resultID
    }
}
